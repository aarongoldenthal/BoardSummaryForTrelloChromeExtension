# Changelog

## Unreleased

### Fixed

- Updated to latest dependencies, including resolving vulnerability CVE-2022-25858 (dev only)

### Miscellaneous

- Setup [renovate](https://docs.renovatebot.com/) for dependency updates (#159, #164)
- Added reporter `pa11y-ci-reporter-cli-summary` to `pa11y-ci` job (#169)
- Replaced `del` with `rimraf` in build. (#176)
- Update product site to latest dependencies. (#178)

## Version 1.9.6 (2021-03-21)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Updated CI pipeline to optimize dependent jobs (#151), add depcheck job (#152), run Pagean 4.4 with a static server (#154), add new CSS linting rules (#155), and optimize for schedules (#156)

## Version 1.9.5 (2020-11-27)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Updated CI pipeline to leverage simplified include syntax in GitLab 13.6 (#149) and GitLab Releaser (#150)

## Version 1.9.4 (2020-06-13)

### Changed

- Updated CI pipeline to test external files from product site (#145)
- Updated CI pipeline with an option to skip deploy to Chrome Store (#147)

### Fixed

- Updated product site dependencies to resolve vulnerabilities (#146)

## Version 1.9.3 (2020-06-11)

### Fixed

- Fixed formatting issues with [Trello card covers](https://blog.trello.com/card-covers-and-colors).

## Version 1.9.2 (2020-06-07)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## Version 1.9.1 (2020-04-20)

### Fixed

- Updated documentation to be compatible with Hugo 0.67.0
- Updated to latest dependencies, including resolving vulnerabilities

## Version 1.9.0 (2020-01-01)

### Changed

- Added support for nested boards on the user's cards page (https://trello.com/me/cards) (#85)

### Fixed

- Fixed issue with Nested Board creation popover closing on any click, preventing nesting new boards (#138)
- Fixed formatting issue on team pages for boards with long names (#111)
- Update to latest dependencies

### Miscellaneous

- Added documentation on required Chrome permissions (#102)
- Automated extension release from CI (#130)

## Version 1.8.7 (2019-11-25)

### Fixed

- Updated to latest dependencies, including vulnerability fixes (#127, #131)
- Fixed errors where creating a new nested board with checklists/items was failing when converting checkitems to cards or moving cards to the new board (#108)

### Miscellaneous

- Continuous integration pipelines updates
  - Added jobs to lint HTML, CSS, and markdown (with applicable code and documentation updates per those standards)
  - Added [accessibility](https://pa11y.org/) and [other browser](https://www.npmjs.com/package/page-load-tests) testing (#123, #135)
  - Updated entire pipeline to the latest [templates](https://gitlab.com/gitlab-ci-utils/gitlab-ci-templates) (#128)
  - Updated project to use custom eslint configuration module (#25)
- Fixed formatting issues in [documentation](https://boardsummary.gitlab.io/BoardSummaryForTrelloChromeExtension/docs/) and [changelog](https://boardsummary.gitlab.io/BoardSummaryForTrelloChromeExtension/changelog/) pages (#120)

## Version 1.8.6 (2019-05-26)

### Fixed

- Fixed icon formatting issue with latest Trello update (#122).
- Fixed issue where nested boards count not be created after latest Trello update (#124).

### Changed

- Refactored CI pipeline to use GitLab and custom templates where possible (#116, #119, #121, #125).

## Version 1.8.5 (2019-01-24)

### Fixed

- Fixed an issue where boards were being mistakenly identified as parent boards.

### Miscellaneous

- Created new project page that contains an overview of the extension and all documentation previously in the wiki, which can be found at https://boardsummary.gitlab.io/.
- Migrated build to latest Gulp release (v4) and Terser for minification.

## Version 1.8.4 (2019-01-08)

### Fixed

- Fixed an issue where updates to the Trello home boards page was causing an error and the team board sections were not showing board summaries.

## Version 1.8.3 (2019-01-06)

### Fixed

- Fixed an issue where creating a new nested board with checklists/items was failing when converting checkitems to cards or moving cards to the new board (a temporary fix until a more significant architectural change can be made).
- Fixed an issue where the nested board popup menu no longer identified an existing nested board.
- Fixed an issue where Trello-initiated page changes were not properly identified.

### Miscellaneous

- Moved notifications from [Google+](https://plus.google.com/u/1/117074922438585101377) to [Twitter](https://twitter.com/boardsummaryapp).
- Updated to the latest version of dependencies and GitLab static code analyses.

## Version 1.8.2 (2018-07-22)

### Fixed

- Final updates to Board Summary formatting issues with new Trello board/team page layout.
- Fixed issue with wide home page setting not being shown on options page.

## Version 1.8.1 (2018-07-17)

### Fixed

- Fixes Board Summary formatting issues with new Trello board/team page layout.

## Version 1.8.0 (2018-07-11)

### Added

- Added an option to reformat the new Trello Board home page to display wider boards, which resolves some issues with Board Summary formatting.
- Added an option to display board percent complete in the Board Summary (based on active and archived cards).
- Added an option to display abbreviated due date badges in Board Summary (just colored badges with numbers, no words).
- Setup GitLab CI pipeline

### Fixed

- Added formatting for parent links on backgrounds that Trello considers "gray" (in addition to "light" and "dark", which were already covered).

## Version 1.7.0 (2017-12-19)

### Added

- Added new options to allow additional control of the data included in nested board cards. The previous option was to show/hide all other card content (all or nothing). The new options include separate settings to show/hide labels, badges, and members. There's also an option to hide the description badge (if badges are displayed), since there's always a description for nested boards.
- A new tab with release notes is now opened when the Board Summary for Trello extensions is installed or updated.

### Fixed

- Board parents were updated to only show unique parent boards.
- Fixed logic so that the nested board popover is always closed if it was displayed when the card details window is closed.
- Fixed logic so nested board links are removed if the option is changed to no longer show nested boards.
- Fixed display issues with the nested board icon which occurred in some cases where the display is scaled in Windows.

## Version 1.6.0 (2017-06-27)

### Added

- Added options to set nested board color to a fixed value, based on card labels, or disable icon.
- Update jQuery and incorporated Handlebars for HTML templates.

### Fixed

- Fixed issue found on June 27 where nested boards were not being displayed (Trello updated the card HTML, which caused some errors).

## Version 1.5.1 (2017-03-03)

### Fixed

- Fixed issue where page change during a board summary background refresh resulted in the board summary on the new page not loading initially.

## Version 1.5.0 (2017-02-26)

### Added

- Added complete / total checklist items to board summary (with optional setting)
- Updated list and card labels in board summary to use Trello icons
- Updated logic so any option changes are automatically reflected on all open Trello tabs

## Version 1.4.1 (2017-01-03)

### Fixed

- Fixed an issue where Chrome was aborting some Trello requests (captured in Chrome issue https://bugs.chromium.org/p/chromium/issues/detail?id=677828)

## Version 1.4.0 (2017-01-01)

### Fixed

- Fixed several issues that could cause Trello requests to exceed the request limit, which is believed to be the issue some people have seen with being logged out of Trello
  - Fixes were made to meter Trello requests at a rate based on the total number to stay within the Trello limit, and to fix several bugs that could cause multiple page processing loops
  - With these fixes the initial page load with a lot of boards will be slower to stay within the limit
- Fixed bug where board summaries were not updated when a section was added to or removed from the home page (e.g. when starring the first board or adding a new group)

## Version 1.3.0 (2016-12-11)

### Added

- Added link on board pages to parent board(s), i.e. any board that contains the current board as a nested board

## Version 1.2.0 (2016-11-30)

### Added

- Added cards completed to board summary (with optional setting) after Trello added the capability to mark due dates completed
- Updated nested board popover to group boards by teams
- Updated board summary to display red Trello icon if board cannot be found (i.e. deleted) or if the user does not have permission

## Version 1.1.3 (2016-11-15)

### Fixed

- After talking to Trello they fixed the original issue where cards created from checklist items were not moved when creating a new nested board, so reverted back to original logic

## Version 1.1.2 (2016-11-13)

### Fixed

- Fixed issue where cards created from checklist items were not moved when creating a new nested board
- Updated logic to only react to applicable card changes, not all card changes (a Trello change has starting causing card updates every 10 seconds)

## Version 1.1.1 (2016-11-12)

### Fixed

- Fixed display issue with board collection and change collection icons (for Trello business/enterprise)
- Fixed issue where false error messages were added to UI, but never removed

## Version 1.1.0 (2016-07-17)

### Added

- Added capability to create a new nested board from a card, including converting CheckLists/CheckItems to Lists/Cards on the new board
- Added warning that description is overwritten when a nested board is created

### Fixed

- Other minor bug fixes

## Version 1.0.0 (2016-07-02)

### Fixed

- Updated all Trello requests to optimize API use, which increased speed significantly (fewer calls, each of which returns less data)
- Fixed bug with card click handler not being removed if a nested board was changed back to a regular card
- Updated logic to resize home page boards when new board added or window resized

## Version 0.9.3 (2016-06-21)

### Fixed

- Updated card edit logic to make add nested board button appear without delay
- Updated positioning logic for popover to ensure it's completely displayed on the page

## Version 0.9.2 (2016-06-13)

### Fixed

- Fixed issue with Trello rendering board URLs with a trailing '/'

## Version 0.9.1 (2016-06-12)

### Fixed

- Fixed issue with nested board button on card edit page being added multiple times

## Version 0.9.0 (2016-06-12)

### Added

- Added button/popup on card edit page to create a nested board from any existing open board
- First public release of extension in Chrome store

## Version 0.8.0 (2016-06-07)

### Added

- Added logic to monitor for page changes and take appropriate actions. If boards/cards are added or changed, then refresh board summary for that board/card.

## Version 0.7.2 (2016-05-30)

### Added

- Added titles to board summary badges
- Removed remaining debug logging statements

## Version 0.7.1 (2016-05-29)

### Added

- Added new option for board summary refresh time
- Created new Board Summary for Trello icon
- Upgraded to jQuery 2.2.4
- Added handling for closed nested board
- Refactored code to isolate configuration data and adopt module pattern

### Fixed

- Fixed several formatting errors
- Added logic to remove board summary if a nested board is changed back to a normal card

## Version 0.6.0 (2016-05-14)

### Added

- Initial beta release with board summary displayed on board and home pages.
- First private release of extension in Chrome store
