# Board Summary for Trello Chrome Extension

The Board Summary for Trello extension retrieves and displays summary data for Trello boards.  The board summary can include the number of active lists/cards/checklists in the board and a summary of due dates for cards in the board (past due/due now/due today/due soon/completed).  Option settings are available to configure board summaries as desired.  The extension also allows for creating nested boards, i.e. cards that reference other boards.  Details on use can be found in the [documentation](https://boardsummary.gitlab.io/BoardSummaryForTrelloChromeExtension/docs/).

The extension can be installed [here](https://chrome.google.com/webstore/detail/board-summary-for-trello/aelhmieiiecnjpaklhmbbefkgdpelobf).
