{
    "title": "Documentation",
    "toc_class": ""
}

# Board Summary for Trello Chrome Extension

The Board Summary for Trello extension retrieves and displays summary data for Trello boards.  The board summary can include the number of active lists/cards/checklists in the board and a summary of due dates for cards in the board (past due/due now/due today/due soon/completed).  Option settings are available to configure board summaries as desired.  The extension also allows for creating nested boards, i.e. cards that reference other boards.

## Trello Home Page / Team Board Pages

On the Trello home page or team board pages summary data for each of the boards is retrieved and displayed, as shown below.  If there are many and/or large boards on this page there may be a noticeable delay while the board summary data is processed.

![Board Summary on Home Page](../images/docs/HomePage_640_400.png)

## Board Pages and Nested Boards

The Board Summary for Trello extension allows the creation of nested boards, i.e. cards whose description links to other boards, and the extension then retrieves and displays the desired data for the nested board (per the configured options).  If errors are encountered retrieving board summary data then they are displayed instead.  The extensions also checks for any parent boards, i.e. boards that have the current board as a nested board, and displays links to those boards.  Details for all of these cases, with example images, are included below.

![Nested Boards](../images/docs/NestedBoard_640_400.png)

Note that nested boards are only a visual representation on that board's page, which means that any summary of the parent board only includes data from that board itself, not any nested boards.

If desired, the displaying of nested boards can be disabled via the Options.

### Nested Boards

The default nested board layout is displayed in most cases - for all nested boards that are not closed and no errors were encountered retrieving the nested board data.  A Trello icon is added next to the card name to indicate that it is a nested board and the applicable board summary is displayed below the name (per the configured board summary Options).  Clicking on the card also links to the nested board instead of opening the card details.  The card details can be viewed by highlighting the card and hitting Enter.  By default, all other Trello icons displayed on the card are also hidden (e.g. labels, comments, description checklist items), but this can be configured via the Options.

![Normal Board](../images/docs/NormalBoard.png)

If the nested board is closed (but not deleted), then a closed board message is displayed instead of the board summary.  All other nested board behavior is the same.

![Closed Board](../images/docs/ClosedBoard.png)

### Nested Board Errors

If an error is encountered in retrieving the nested board data, the extension displays data appropriate for the type of error encountered.

If Trello responds that a board cannot be found (i.e. the request receives an HTTP 404 Not Found response), the missing or deleted board layout is shown.  This is typically the case when a nested board is deleted.  Since it is an error a faded Trello icon is added next to the card name to indicate that it is a nested board with an error, but other card changes are not made (e.g. clicking the card still opens the card details).  Hovering over the Trello icon changes it to red and indicates the specific error.

![Missing or Deleted Board](../images/docs/DeletedBoard.png)
![Missing or Deleted Board](../images/docs/DeletedBoardHover.png)

If Trello responds that the user does not have access to the nested board (i.e. the request receives an HTTP 403 Forbidden response), the unauthorized board layout is shown.  This can be the case if a user is removed from a private board, removed from a team, a public board is made private, etc.  The layout in this case is the same as the missing or deleted board layout (i.e. faded Trello icon, hovering over the icon changes it to red and indicates the specific error).

![Unauthorized Board](../images/docs/UnauthorizedBoard.png)
![Unauthorized Board](../images/docs/UnauthorizedBoardHover.png)

If there is any other error retrieving data from Trello (e.g. a network error, Trello servers down) an error message is displayed in the card.  The card is not otherwise changed, so if the nested board data was already displayed then it remains, otherwise the original card remains.

![Card Error](../images/docs/CardError1.png)
![Card Error](../images/docs/CardError2.png)

### Nested Board Icon Colors

By default, the nested board icons are blue, but via Options the icons can be set to other specific colors, colored based on the label applied to that card, or not displayed at all.  The following is an example showing all possible nested board icon colors (the nominal icons in the Boards lists, and the faded error icons in the Errors lists).

![All Nested Board Icon Colors](../images/docs/AllNestedBoardIconColors.png)

### Parent Boards

The extensions also checks for any parent boards, i.e. boards that have the current board as a nested board, and displays links to those boards at the top of the page.  If multiple parent boards are found they are displayed, listed alphabetically by the name of the board.

![Parent Board](../images/docs/ParentBoard1.png)
![Parent Board](../images/docs/ParentBoard2.png)

## Creating Nested Boards

The Board Summary for Trello extension allows for creating nested boards, i.e. cards that reference other boards.  These cards then include the board summary for that board, and clicking on the card links to the nested board.  If the current board is nested on another board (or multiple boards), links are displayed at the top of the page to all parent boards.

Nested boards are created by setting the card description to the URL of another board, which allows nested boards to be usable in other browsers, mobile apps, etc.  This process is facilitated by the capabilities in the extension as detailed below.

The examples below show how to create a nested board linking to a new board, or to an existing board.

### Nested Board Linking to a New Board

The process for creating a nested board linking to a new board is best illustrated with an example.  The image below shows the original card to be used.  In this case the card includes two checklists.

![Card Before Nested Board](../images/docs/CardBeforeNestedBoard.png)

Clicking the "Nested Board" button displays the Create Nested Board popup with several options.  The default is to create a new board, which can be seen selected below.

![Card With Create Nested Board Popover](../images/docs/CardWithCreateNestedBoardPopover.png)

There are several options available when create a nested board linking to a new board:

- Keep board settings - If checked, board settings from the current board (team, background, card aging, card cover images, permissions, allow self-join, etc) are copied to the new board.
- Move checklists/items to lists/cards - If checked, any checklists/items are converted to lists/cards on the new board.  Any completed checklist items are not moved.  This allows a card to be a placeholder for a new board, and when ready the the card/checklists/items can be promoted to a board/lists/cards.
- Delete empty checklists after move - If checked, any empty checklists (i.e. with no items) are deleted.  Any checklists with completed items are retained.

Clicking "Nest Board" creates the new board titled after the original card, and if selected moves any checklists/items to the board.  The image below shows the results from the example above:

- The card description has been changed to a link to the new board (which Trello recognizes and displays the Trello icon)
- Checklists/items that were not completed were moved to the new board
- The "Options" checklist was deleted since all items were moved, therefore it was empty afterwards
- The "Financing" checklist remains with the completed "Mom and Dad" item

![Card After Nested Board](../images/docs/CardAfterNestedBoard.png)

The new "Research new car" board that was created is shown below:

![New Board](../images/docs/NewBoard.png)

As described above, you can see that:

- The board title is taken from the card title
- Lists were created for all checklists (Options and Financing), retaining their original order
- All incomplete items were moved to the appropriate lists, retaining their original order
- A link is added at the top of the page to the parent board

When checklist items are converted, they are done so using Trello's built-in capabilities to convert checklist items to cards and move cards between boards, so the history is maintained as shown below.  The card activity shows that the card was created from a checklist item on the "Research new car" card, then moved from the "To-Do List" board.

![New Card With History](../images/docs/NewCardWithHistory.png)

### Nested Board Linking to an Existing Board

To nest an existing board, simply select the board from the given list, as shown below.  The list includes all of your boards - personal boards first, then boards grouped by team.  The options available for creating a new board are not applicable when nesting an existing board.

![Nest Existing Board](../images/docs/NestExistingBoard.png)

## Options

The following options are available to configure the Board Summary for Trello extension.  Options are synced between browsers with other settings if enabled in Chrome.

### Global Board Summary Options

The global board summary options control general functionality on all Trello pages:

![Global Board Summary Options](../images/docs/GlobalBoardSummaryOptions.png)

- Show board summary on home page - If checked (default), board summaries are shown for all boards on the Trello home page and team pages.
- Show board summary for nested boards - If checked (default), board summaries are shown for all nested boards on Trello board pages.
- Update board summary every 5 minutes - Time interval to update board summaries on any page (default 5 minutes, minimum 2 minutes).
- Display boards in wide format on boards home page - Reformats the new Trello Board home page to widen the page and display 3 boards per row, which allows a wider board and resolves some issues with Board Summary formatting in the narrower 4 boards per row format, as shown below:

![Wide Format Boards](../images/docs/WideFormatBoards.png)

### Board Summary Content Options

The board summary content options control how board summaries are displayed on any page.  If checked, that element is displayed.  The default values are shown below.

![Board Summary Content Options](../images/docs/BoardSummaryContentOptions.png)

Board summary content elements on Trello home page/team pages and nested boards on board pages:

![Home Board Summary](../images/docs/HomeBoardSummary.png)
![Nested Board Summary](../images/docs/NestedBoardSummary.png)

Legend:

1. Show number of active lists
2. Show number of active cards
3. Show number of complete / total checklist items
4. Show percentage of completed (i.e. archived) cards
5. Show number of cards past due (> 36 hours ago)
6. Show number of cards due now (last 36 hours)
7. Show number of cards due today (next 24 hours)
8. Show number of cards due in the following 7 days (configurable - default 7 days, minimum 1 day)
9. Show number of cards with due dates complete

Note that the "percentage of completed cards" assumes only cards that are visible on the board are not complete.  This means that cards that are not archived, but are in lists that are archived, are considered complete.

The last option, "show abbreviated due date badges" removes the text from the due date badges to just show the counts, as seen below.

![Abbreviated Due Date Badges](../images/docs/AbbreviatedDueDateBadges.png)

### Nested Board Options

The nested board options control the behavior of nested boards:

![Nested Board Options](../images/docs/NestedBoardOptions.png)

- Nested board icon color - Controls the color applied to the nested board icons, with the following values (see above for more details on nested boards):
  - Default color - Nested board icons are colored blue (default).
  - Card label - Nested board icons are colored based on the first label on that card.
  - Fixed color - Nested board icons are set to the specified color (all Trello label colors are available).
  - No icon - No icons are displayed for nested boards
- Show card labels for nested boards - If checked, displays card labels for nested boards.  The default is unchecked (card labels are hidden).
- Show card badges for nested boards - If checked, displays card badges (subscribed, description, comments, attachments, checklists) for nested boards.  The default is unchecked (card badges are hidden).
- Hide description badge for nested boards - If checked (default), card description badges are hidden for nested boards (since nested boards use the card description, there is always a description for nested boards).  This option is only enabled if card badges are shown.
- Show card members for nested boards - If checked, displays card members for nested boards.  The default is unchecked (card members are hidden).

The example below shows both all card content hidden for nested boards (the default) as well as all card content shown except description.

![Hide Card Content](../images/docs/HideCardContent.png)

## Required Permissions for Chrome Extension

When adding the Board Summary for Trello extension, Chrome will prompt that you are granting the extension permission to:

- Read and change your data on trello.com
- Read your browsing history

The following details how this data is used.  Note that any data is only used locally (i.e. in your browser) for the extension to render content.  It is not saved or sent anywhere else for any other purposes.

### Read and Change Your Data on Trello.com

The Board Summary for Trello extension reads data on your Trello pages, and based on that data it retrieves additional data from Trello (e.g. board summaries), and then writes that data to the page.  So, for this to work, it must read data from and write data to pages, but that permission is limited to only trello.com pages.

### Read Your Browsing History

The Board Summary for Trello extension requires the browsing history data for two reasons:

- Updating trello.com pages
- Updating pages after the extension options are changed

#### Updating Trello.com Pages

Trello functions as a single page application, so as you navigate through the site the page is not reloaded, instead the appropriate content is rendered and the history updated.  Since the page is not reloaded when content changes, the extension uses changes in the current page URL (from the browsing history) to determine when you navigate to a new page and the render the new content based on the type of page (e.g. home page, team page, board page, card page).  This is only required for trello.com pages.

#### Updates Pages After the Extension Options Are Changed

When the extension options are saved, the extension will re-render content on any open Trello pages to reflect any changes. To do this it must check every open tab to determine which pages are trello.com, which requires access to the browsing history.
